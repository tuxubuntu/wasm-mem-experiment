dev: build-module tests
build-module:
	cd test/module && cargo build --target wasm32-unknown-unknown --release
tests:
	cargo test --release --all -- --nocapture