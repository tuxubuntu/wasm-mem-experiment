#![cfg(test)]

use super::*;
use std::time::Instant;
use wasmer_runtime::{imports, instantiate};

// Create an absolute path to the Wasm file
const WASM_FILE_PATH: &str = "../target/wasm32-unknown-unknown/release/module.wasm";

fn read_source<'a>(path: &'a str) -> Vec<u8> {
	use std::path::PathBuf;
	use std::fs::File;
	use std::io::prelude::*;
	let path = PathBuf::from(path);
	let mut file = File::open(path).unwrap();
	let mut contents = Vec::new();
	file.read_to_end(&mut contents).unwrap();
	contents
}

#[test]
fn simple() {
	let wasm_vec = read_source(WASM_FILE_PATH);
	let import_object = imports! {};
	let instance = instantiate(&wasm_vec, &import_object).unwrap();

	let x = Module::from(&instance);

	x.call("demo", vec![3, 2, 1]).unwrap();
}

#[test]
fn large() {
	let wasm_vec = read_source(WASM_FILE_PATH);
	let import_object = imports! {};
	let instance = instantiate(&wasm_vec, &import_object).unwrap();

	let x = Module::from(&instance);

	// x.set_chung_size(1024 * 1024);

	let len = 1024 * 1024;
	let count = 10;

	let total = Instant::now();

	for _ in 0..count {

		let start = Instant::now();

		let data = vec![0; len];

		let res = x.call("demo", data.clone()).expect("large vec");

		let duration = start.elapsed();

		println!("transfered {:?} bytes on {:?}.", res.len(), duration);
		assert_eq!(len + 3, res.len());
		assert!(res[0..len] == data[..]);
	}
	let duration = total.elapsed();
	println!("total sended {:?} bytes time {:?}.", len * count, duration);
}
