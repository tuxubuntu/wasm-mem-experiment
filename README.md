
# Pass `Vec<u8>` between `wasmer-runner` and `.wasm`


## Agent

Example in `./test/module`

```rust

// wasmem-agent = "*"

#[no_mangle]
pub fn demo() -> usize {
	// Let's get the passed string from our passed bytes
	let mut passed_data = wasmem_agent::read_msg().unwrap();

	passed_data.push(11);
	passed_data.push(12);
	passed_data.push(13);

	// Let's write the new string back to our buffer
	wasmem_agent::write_msg(passed_data)
}

```

## Host

Example in `wasmem-host/src/tests.rs`

```rust

// wasmem-host = "*"

// load wasm module in wasmer

use wasmer_runtime::{error, imports, instantiate, Array, Func, WasmPtr, Memory};
use wasmer_runtime::Instance;

const WASM_FILE_PATH: &str = "./module.wasm";

let wasm_source = read_source(WASM_FILE_PATH);
let import_object = imports! {};
let instance = instantiate(&wasm_source, &import_object).unwrap();

// use wasmem

use wasmem_host::Module;

let x = Module::from(&instance);

let res = x.call("demo", vec![3, 2, 1]).unwrap();

```
