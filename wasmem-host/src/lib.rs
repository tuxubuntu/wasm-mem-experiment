
mod tests;
mod module;

pub use crate::module::Module;
use wasmer_runtime::{Array, Func, WasmPtr};

type Warr = WasmPtr<u8, Array>;
type Getter<'a> = Func<'a, (), WasmPtr<u8, Array>>;
type Collector<'a> = Func<'a, u32, u32>;
type Loader<'a> = Func<'a, u32, u32>;

pub const DEFAULT_BUFFER_SIZE: usize = 1024 * 64;
