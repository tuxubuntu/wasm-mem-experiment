
// wasm

// Function to get the string from the buffer and add the text to it
#[no_mangle]
pub fn add_wasm_is_cool(passed_string_length: usize) -> usize {
	// Let's get the passed string from our passed bytes
	let mut passed_data =
		unsafe { Vec::from(&WASM_MEMORY_BUFFER[..passed_string_length]) };

	passed_data.push(11);
	passed_data.push(12);
	passed_data.push(13);

	let passed_data_len = passed_data.len();

	// Let's write the new string back to our buffer
	unsafe {
		WASM_MEMORY_BUFFER[..passed_data_len].copy_from_slice(&passed_data);
	}

	// Return the length of the new string for the host to fetch it out of memory
	passed_data_len
}



// * https://github.com/wasmerio/docs.wasmer.io/blob/master/docs/runtime/rust-integration/examples/passing_data.rs
// * https://github.com/wasmerio/docs.wasmer.io/blob/master/docs/runtime/rust-integration/wasm/passing-data-guest/src/lib.rs

// Import the wasmer runtime so we can use it
use wasmer_runtime::{error, imports, instantiate, Array, Func, WasmPtr, Memory};
use wasmer_runtime::Instance;

use std::time::Instant;

type Warr = WasmPtr<u8, Array>;


fn read_vec<'a>(ptr: &Warr, memory: &'a Memory, str_len: u32) -> Option<&'a [u8]> {
	let offset = ptr.offset() as usize;
	if offset + str_len as usize > memory.size().bytes().0 {
		return None;
	}
	let ptr = unsafe { memory.view::<u8>().as_ptr().add(offset) as *const u8 };
	let slice: &[u8] = unsafe { std::slice::from_raw_parts(ptr, str_len as usize) };
	Some(slice)
}

fn get_buffer<'a>(getter: &Getter<'a>) -> Warr {
	getter.call().unwrap()
}


// Our entry point to our application
fn demo() -> error::Result<()> {

	let wasm_vec = read_source(WASM_FILE_PATH);

	let import_object = imports! {};

	let instance = instantiate(&wasm_vec, &import_object)?;

	// Lets get the context and memory of our Wasm Instance
	let wasm_instance_context = instance.context();
	let wasm_instance_memory = wasm_instance_context.memory(0);

	let getter = get_getter(&instance);
	let wasm_buffer_pointer = get_buffer(&getter);


	let original = vec![1,2,3];
	let original_len = original.len() as u32;

	// We deref our WasmPtr to get a &[Cell<u8>]
	let memory_writer = wasm_buffer_pointer
		.deref(wasm_instance_memory, 0, original_len)
		.unwrap();
	for (i, b) in original.into_iter().enumerate() {
		memory_writer[i].set(b);
	}

	// Let's call the exported function that concatenates a phrase to our string.
	let add_wasm_is_cool: Func<u32, u32> = instance
		.func("add_wasm_is_cool")
		.expect("Wasm is cool export");
	let new_string_length = add_wasm_is_cool.call(original_len).unwrap();

	// Get our pointer again, since memory may have shifted around
	let new_wasm_buffer_pointer = get_buffer(&getter);
	// let new_wasm_buffer_pointer = get_wasm_memory_buffer_pointer.call().unwrap();

	let res = read_vec(&new_wasm_buffer_pointer, wasm_instance_memory, new_string_length);

	dbg!(res);

	// Return OK since everything executed successfully!
	Ok(())
}

#[cfg(test)]
mod tests {
	use super::*;
	#[test]
	fn demo() {
		super::demo().unwrap();
	}
}

