
pub const DEFAULT_BUFFER_SIZE: usize = 1024 * 64;

#[macro_export]
macro_rules! init {
	($size:expr) => {
		// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
		// allocator.
		#[cfg(feature = "wee_alloc")]
		#[global_allocator]
		static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

		// Create a static mutable byte buffer.
		// We will use for passing memory between our host and wasm.
		// NOTE: global `static mut` means we have to access it with unsafe
		// and manually ensure that only one mutable reference exists to it at a time
		// but for passing memory between a host and wasm should be fine if we know the
		// host won't share this Wasm's linear memory with another instance.
		const WASM_MEMORY_BUFFER_SIZE: usize = $size;
		static mut WASM_MEMORY_BUFFER: [u8; WASM_MEMORY_BUFFER_SIZE] = [0; WASM_MEMORY_BUFFER_SIZE];

		static mut WASM_INNER_MESSAGE: Option<Vec<u8>> = None;
		static mut WASM_OUTTER_MESSAGE: Option<Vec<u8>> = None;

		// Function to return a pointer to our buffer
		// in wasm memory
		#[no_mangle]
		pub fn get_weealloc_wasm_memory_buffer_pointer() -> *const u8 {
			unsafe { WASM_MEMORY_BUFFER.as_ptr() }
		}

		// collect passed data chunk and return totaly received size
		#[no_mangle]
		pub fn colect_weealloc_chunk(size: usize) -> usize {
			// Let's get the passed bytes
			let passed_data = unsafe { Vec::from(&WASM_MEMORY_BUFFER[..size]) };
			unsafe {
				if WASM_INNER_MESSAGE.is_none() {
					WASM_INNER_MESSAGE.replace(passed_data);
				} else {
					WASM_INNER_MESSAGE.as_mut().unwrap().extend(passed_data)
				}
			};

			unsafe { WASM_INNER_MESSAGE.as_mut().unwrap().len() }
		}

		// load in buffer chunk from outer message (starts from byte) return count of loaded bytes
		#[no_mangle]
		pub fn load_next_weealloc_chunk(start: usize) -> usize {
			// Let's get the passed bytes
			let total_len = unsafe { WASM_OUTTER_MESSAGE.as_ref().unwrap().len() };

			let chunk_len = if total_len - start < $size {
				total_len % $size
			} else {
				$size
			};

			unsafe {
				WASM_MEMORY_BUFFER[0..chunk_len].copy_from_slice(
					&WASM_OUTTER_MESSAGE.as_ref().unwrap()[start..(start + chunk_len)]
				);
			};

			chunk_len
		}
	};
	() => {
		init!(DEFAULT_BUFFER_SIZE);
	};
}

init!();

pub fn read_msg() -> Option<Vec<u8>> {
	unsafe { WASM_INNER_MESSAGE.replace(vec![]) }
}

pub fn write_msg(buffer: Vec<u8>) -> usize {
	let len = buffer.len();
	unsafe { WASM_OUTTER_MESSAGE.replace(buffer) };
	len
}
