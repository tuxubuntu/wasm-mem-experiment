
#[no_mangle]
pub fn demo() -> usize {
	// Let's get the passed string from our passed bytes
	let mut passed_data = wasmem_agent::read_msg().unwrap();

	passed_data.push(11);
	passed_data.push(12);
	passed_data.push(13);

	// Let's write the new string back to our buffer
	wasmem_agent::write_msg(passed_data)
}
