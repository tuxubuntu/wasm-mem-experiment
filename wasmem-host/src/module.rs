
// Import the wasmer runtime so we can use it
use wasmer_runtime::{error, Func, Memory};
use wasmer_runtime::Instance;

use crate::DEFAULT_BUFFER_SIZE;
use crate::Getter;
use crate::Collector;
use crate::Loader;
use crate::Warr;

pub struct Module<'a> {
	instance: &'a Instance,
	memory: &'a Memory,
	getter: Getter<'a>,
	collector: Collector<'a>,
	loader: Loader<'a>,
	chunk_size: usize,
}

impl<'a> From<&'a Instance> for Module<'a> {
	fn from(instance: &'a Instance) -> Self {
		// Let's get the pointer to the buffer defined by the wasm module in the wasm memory.
		// We use the type system and the power of generics to get a function we can call
		// directly with a type signature of no arguments and returning a WasmPtr<u8, Array>
		const FN_MEMORY_BUFFER: &str = "get_weealloc_wasm_memory_buffer_pointer";
		const FN_COLLECT: &str = "colect_weealloc_chunk";
		const FN_LOAD_NEXT: &str = "load_next_weealloc_chunk";
		let getter = instance
			.func(FN_MEMORY_BUFFER)
			.unwrap();
		let collector = instance
			.func(FN_COLLECT)
			.unwrap();
		let loader = instance
			.func(FN_LOAD_NEXT)
			.unwrap();
		// Lets get the context and memory of our Wasm Instance
		let wasm_instance_context = instance.context();
		let memory = wasm_instance_context.memory(0);
		Module {
			instance,
			memory,
			getter,
			collector,
			loader,
			chunk_size: DEFAULT_BUFFER_SIZE,
		}
	}
}


impl<'a> Module<'a> {
	pub fn call(&self, name: &str, bytes: Vec<u8>) -> error::Result<Vec<u8>> {
		// use std::time::Instant;

		// let start = Instant::now();
		let func: Func<(), u32> = self.instance.func(name)?;
		// println!("\n\n\n>>> getting Func {:?}", start.elapsed());

		// let start = Instant::now();
		self.write(bytes)?;
		// println!(">>> Writed {:?}", start.elapsed());

		// let start = Instant::now();
		let result_len = func.call()?;
		// println!(">>> Called {:?}", start.elapsed());

		// let start = Instant::now();
		let mut res = vec![0; result_len as usize];
		self.read(&mut res).unwrap();
		// println!(">>> Readed {:?}", start.elapsed());

		Ok(res)
	}
	pub fn set_chung_size(&mut self, new_size: usize) {
		self.chunk_size = new_size;
	}
}


impl<'a> Module<'a> {
	fn get_buffer(&self) -> error::Result<Warr> {
		Ok(self.getter.call()?)
	}
	fn write(&self, bytes: Vec<u8>) -> error::Result<usize> {

		let size = self.chunk_size;

		let wasm_buffer_pointer = self.get_buffer()?;

		let total_len = bytes.len();

		let mut len = 0;

		while len != total_len {
			let chunk_len = if total_len - len < size {
				total_len % size
			} else {
				size
			};

			// println!("writing {:?}", chunk_len);

			// We deref our WasmPtr to get a &[Cell<u8>]
			let memory_writer = wasm_buffer_pointer
				.deref(self.memory, 0, chunk_len as u32)
				.unwrap();

			for (i, b) in bytes[len..(len + chunk_len)].iter().enumerate() {
				memory_writer[i].set(b.clone());
			}

			// for (hole, i) in memory_writer.iter().zip(bytes.drain(len..(len + chunk_len))) {
			// 	hole.set(i);
			// }

			self.collector.call(chunk_len as u32)?;
			len += chunk_len;
		}

		Ok(total_len)
	}
	fn read(&self, buffer: &mut Vec<u8>) -> error::Result<()> {

		let total_len = buffer.len();

		let mut len = 0;

		while total_len != len {
			let chunk_len = self.loader.call(len as u32)? as usize;
			if chunk_len == 0 {
				break;
			}
			// Get our pointer again, since memory may have shifted around
			let new_buffer_ptr = self.get_buffer()?;


			let offset = new_buffer_ptr.offset() as usize;
			if offset + chunk_len > self.memory.size().bytes().0 {
				return Ok(());
			}
			let new_buffer_ptr = unsafe { self.memory.view::<u8>().as_ptr().add(offset) as *const u8 };
			let slice: &[u8] = unsafe { std::slice::from_raw_parts(new_buffer_ptr, chunk_len) };

			// println!("readed {:?}", chunk_len);

			buffer[len..(len + chunk_len)].copy_from_slice(slice);

			len += chunk_len;

		}

		Ok(())
	}
}
